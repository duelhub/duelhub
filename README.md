# DuelHub

## Product name
DuelHub  

## List of team members

- Sadulaeva Teona  

- Rezunik Lyudmila  

- Shchukin Vladislav  

## Product description

DuelHub – платформа, на которой игроки могут договориться о "поединках" в различных компьютерных играх. Представляет собой веб-приложение.  

### Scope

Существующие платформы часто фокусируются на случайном подборе игроков и не предоставляют возможности для организации турниров или соревнований, которые могли бы быть настроены в соответствии с предпочтениями игроков. Это может привести к неудовлетворительному игровому опыту, когда игроки с разными уровнями навыков и интересами попадают в одну и ту же игру.  

Цель, которую преследует данный продукт – предоставление возможности для удобной и гибкой организации проведения онлайн-поединков и турниров в различных компьютерных играх. Целевой аудиторией веб-приложения, таким образом, станут опытные игроки, а также новички, которые хотят сравнить свои навыки с игроками на похожем уровне. Помимо этого, планируется использование платформы организаторами мини-турниров и соревнований.  

Платформа должна реализовывать функциональность для удобного создания команд, поединков, просмотра результатов, а также текущего статуса поединка зрителями.  

## Similar systems

[https://www.faceit.com](https://www.faceit.com/en)  

https://battlefy.com/organize  

https://www.fortnite.com/competitive  

## Feature list

- **Авторизация в ролях.** Возможные роли: игрок, зритель, капитан, администратор. Каждая роль имеет свой набор возможностей. Например, зритель не может вступать в команды, но может наблюдать за ходом игры.  
- **Онлайн-чат для зрителей.** Чат в реальном времени для зрителей, следящих за поединком. История таких чатов не сохраняется.  
- **Создание команд.** Создание виртуальных групп с капитаном и игроками для участия в поединках.  
- **Командный мессенджер**. Осуществление общения внутри команды посредством внутреннего чата с сохранением истории.  
- **Открытые лобби.** Создание открытых для присоединения лобби для команд.  
- **Система подбора соперников.** Умный поиск лобби, основывающийся на навыках и параметрах соперников.  
- **Статистика.** Анализ результатов поединков, сбор статистики, сохранение информации о прошедших матчах.  
- **Система наград.** Награждения игроков, занявших призовые места.  
- **Техническая поддержка.** Круглосуточный чат с администраторами сервиса для решения вопросов, требующих соответствующих прав.  
